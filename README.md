# counter-app-demo : Deploy an Web Application on Amazon EC2 Instance

## Step 01: Launch an Amazon EC2 (Amazon Linux 2) with Security Group (Inbound Port: 80, 22)

## Connect to the EC2 Instance, Install Nginx Web Server configure it for counter web app

### Install and Start the Nginx Service

```shell
sudo yum install nginx
sudo systemctl enable nginx 
sudo systemctl start nginx
sudo systemctl status nginx
```

### Deploy the simple web appliation 
```shell
vim index.html

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Simple Web App</title>
</head>
<body>
    <header>
        <h1>webapp.com</h1>
    </header>
    <main>
	    <p>Welcome to Simple Web Application </p>
        <p> Hello, Mohammad Tanvir</p>
        <p>Congratulations! You've successfully reached the webapp.com server.</p>
    </main>
    <footer>
        <p>Thank you for visiting webapp.com!</p>
    </footer>
</body>
</html>

sudo cp index.html /usr/share/nginx/html
sudo systemctl restart nginx
```


